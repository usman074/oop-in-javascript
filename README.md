# **OOP in Javascript**


# **Table of Contents**

*  [Basic](#markdown-header-basic)
    *  [Four Pillars of OOP](#markdown-header-four-pillars-of-oop)
*  [Objects](#markdown-header-objects)
    *  [basic](#markdown-header-basic_1)
    *  [Factory Functions](#markdown-header-factory-functions)
    *  [Constructor Functions](#markdown-header-constructor-functions)
    *  [Constructor Property](#markdown-header-constructor-property)
    *  [Functions are Objects](#markdown-header-functions-are-objects)
    *  [Value vs Reference Type](#markdown-header-value-vs-reference-type)
    *  [Adding or Removing Properties](#markdown-header-adding-or-removing-properties)
    *  [Enumerating Object Properties](#markdown-header-enumerating-object-properties)
    *  [Abstraction](#markdown-header-abstraction)
    *  [Getter & Setter](#markdown-header-getter-setter)
*  [ES6 Classes](#markdown-header-es6-classes)
    *  [ES6 Classes](#markdown-header-es6-classes_1)
    *  [Static Methods](#markdown-header-static-methods)
    *  [Private Members With Symbols](#markdown-header-private-members-with-symbols)
    *  [Private Members With WeakMap](#markdown-header-private-members-with-weakmap)
    *  [Getter & Setter](#markdown-header-getter-setter_1)
    *  [Inheritance](#markdown-header-inheritance)
    *  [Method Overriding](#markdown-header-method-overriding)
    *  [Polymorphism Link1](https://o7planning.org/en/12231/inheritance-and-polymorphism-in-ecmascript)
    *  [Polymorphism Link2](https://medium.com/@viktor.kukurba/object-oriented-programming-in-javascript-3-polymorphism-fb564c9f1ce8)
*  [Modules](#markdown-header-modules)
    *  [CommonJS Modules](#markdown-header-commonjs-modules)
    *  [ES6 Modules](#markdown-header-es6-modules)



## Basic

* #### **Four Pillars of OOP**

1. Encapsulation

In OOP, we combine a group of related variables and functions into a unit (Class / object). Here variables are referred to as properties and functions are reffered to as methods. This approach is called Encapsulation.

[More on Encapsulation](https://stackify.com/oop-concept-for-beginners-what-is-encapsulation)

2. Abstraction

To hide unnecessary properties and functions from user and restrict their accessbility outside the class / object is Abstraction.

[More on Abstraction](https://www.guru99.com/java-data-abstraction.html)

3. Inheritance

If one or more objects have some properties or methods in common. Instead of repeating the code of those properties and method for each object, what we do is we define those common properties and methods once in an object / class. And all the other objects inherit that object / class to use those properties. It makes the code redundant.

4. Polymorphism

Because of polymorphism we get rid of ugly if-else or switch case statements. E.g, if many objects have same property but they behave differently based on the object calling it. The conventional approach is to use if-else or switch case to handle multiple scenarios depending upon object. But we can use the polymorphism approach here to get rid of conventional approach and implement the code such that it will behave differently based on the object calling.

**[⬆ back to top](#markdown-header-table-of-contents)**

## Objects

Objects can be defined with object literals i.e '{}'. 

Objects can have key value pairs. Values can be of primitive types or reference types.


* #### **Basic**

```javascript
const circle = {
    radius: 1,
    location: {
        X: 1,
        Y: 1
    },
    isVisible: true,
    draw: function () {
        console.log('draw');
    }
}

//initialize circle
circle.draw(); //Method


//output => 'draw'

```

**Note: If function is declared in an object, we should prefer to call it method not fucntion.**

If we want to make a new circle we have to repeat our code.  So instead of repeating our circle code we can change the implementation.

We can modify the implementation in either way:

1.	Factory Functions

2.	Constructor Functions

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Factory Functions**
In factory functions, we just define the object implementation in a function and return that object. And we initialize the object by calling the function.

```javascript
//If key and value name is same, we just write it once as below
function createCircle(radius) {
    return {
        radius,
        draw() {
            console.log('draw');
        }
    }
}

const circle1 = createCircle(1);
console.log(circle1);

//output => {
//   radius: 1,
//   f draw()
//   }

const circle2 = createCircle(2);
console.log(circle2);

//output => {
//   radius: 2,
//   f draw()
//   }

```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Constructor Functions**

In constructor functions, we just define the object implementation in a function named with pascal notation and use ‘this’ keyword to assign values to object. And use ‘new’ keyword to initialize the object.

```javascript
//here ‘this’ refers to the current object, the object in the current context.
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const circle = new Circle(1);

//output => {
//   radius: 1,
//   f draw()
//   }
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Constructor Property**

Every object in javascript has a constructor property. This constructor property refers to the function that construct / create that object.

```javascript
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const another = new Circle(1);
console.log(another.constructor)

//output
// f Circle(radius) {
//	same implementation as above
//	}


function createCircle(radius) {
    return {
        radius,
        draw() {
            console.log('draw');
        }
    }
}

const circle = createCircle(1);
console.log(circle.constructor)

//output
// f Object() {
//	[native code]
//	}

```

As we know creating object with ‘new’ keyword is actually a constructor function approach that’s why first example output is the same function it self.

But in second example we are not creating object with constructor approach. That’s why javascript handle our code at a nutshell and convert the following code:

const circle = createCircle(1);

into this

const circle = new Object(1); //this a constructor function

**Note: when we create object with constructor approach, then 'initialization.constructor' is the same function.
But when we create object with literal / factory function approach, then javascript compiler 
will create  constructor function named Object to create our object.
At the end both constructor functions, i.e our own defined / javascript defined (Object) 
will call the following function to create object.**

*f Function() { [native code] }*

Other built in constructors in javascript are

new String()

new Boolean()

new Number()

When we initialize string, boolean and number with literals, they actually converted into the above constructors to be created.

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Functions are Objects**

```javascript
function Circle(radius) {
    this.radius = radius;
    this.draw = function () {
        console.log('draw');
    }
}

const another = new Circle(1);
//OR
Circle.call = ({}, 1)

//both calls will have same output

//output => {
//   radius: 1,
//   f draw()
//   }

// Above both codes are same, actually when we call with ‘new’ keyword, 
// it converted it into the ‘.call’ function. And ‘this’ keyword refers to that empty object in ‘.call’ function.

// If we have multiple parameter we can call the function as follow

Circle.apply({}, [1, 2, 3]);

```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Value vs Reference Type**

Assigning Value vs Reference type into another variable

```javascript

//example 1

let x = 10;
let y = x;

x = 20;

console.log(x)

//output => 20

console.log(y)

//output => 10

//Here just value of x copy into y, and y has its own separate identity thats why change in x does not
//reflect in y

//example 2

let x = { value: 10};
let y = x;

x.value = 20;

console.log(x)

//output => { value: 20}

console.log(y)

//output => { value: 20}

//Here x is an object. object variable holds the memory address where actual value is stored,
//so when we copy cx in y, the same memory address copied and y also points to the same memory
//when we changed value of x, value in memory changes y is also pointing to same memory thats why
//value of y is also changed

```

Passing Value vs Reference type into a fucntion. i.e, pass by value and pass by reference respectively.

```javascript

//example 1

let number = 10;

function increase(number) {
   number++;
}

increase(number);

console.log(number)

//output => 10

//When we passed number, its value is passed and parameter 'number' in increase function stored 
//its value with a separate identity which has scoped only in function

//expamle 2

let obj = { value: 10};

function increase(obj) {
   obj.value++;
}

increase(obj);

console.log(obj)

//output => {
//   value: 11
//}

//Here when we passed obj, its reference in memory is passed and parameter 'obj' in increase funtion
//stored its reference. When we call increase function, the increment happens to the value in memory
//thats why when we console the obj its value is ncremented.
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Adding or Removing Properties**

```javascript
fucntion Circle(radius) {
    this.radius = radius;
    this.draw = function() {
        console.log('draw');
    }
}

const circle = new Circle(10);

circle.location = {x: 1};

circle['location'] = {x: 1};

const proertyName = 'location';

circle[propertyName] = {x: 1};

//we cant use such keys with dot(.) notation
const proertyName1 = 'center location';

circle[propertyName1] = {x: 1};

const proertyName2 = 'center-location';

circle[propertyName2] = {x: 1};

delete circle.location

delete circle['location'];
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Abstraction**

Abstraction is to hide unnecessary ptoperties and methods.

```javascript
function Circle(radius) {
    this.radius = radius;
    this.defaultLocation = {x: 0, y: 0};
    this.computeOptimumLocation = function(factor) {
        //...
    }
    
    this.draw = function() {
        this.computeOptimumLocation(0.1);
        console.log('draw');
    };
}

const circle = new Circle(10);
circle.draw();

//Here we can also access defaultLocation and computeOptimumLocation with circle object.
//But what if we dont want to allow access to this property and method. Here we will use abstraction to make them private.
//Our code will look like this.

//After Abstraction
function Circle(radius) {
    this.radius = radius;
    //Here we define the property as a part of Circle function not the part of Circle Object
    //thats why it is not associated with Circle object and can not b accessed with CIrcle Object
    let defaultLocation = {x: 0, y: 0};
    
    //Here we define the method as a part of Circle function not the part of Circle Object
    //thats why it is not associated with Circle object and can not b accessed with CIrcle Object
    let computeOptimumLocation = function(factor) {
        //...
    }
    
    this.draw = function() {
        
        //we can access the property and method of Circle function here in this way
        //Accessing these properties is basically a jaavscript feature called closure
        computeOptimumLocation(0.1);
        //defaultLocation
        
        //But properties associated with CIrcle Object can only be accessed with 'this' keyword.
        //this.radius
        
        console.log('draw');
    };
}

const circle = new Circle(10);
circle.draw();
```

**Closure**

Closure is a javascript feature in which inner method has the accessibility to the properties and methods of outer method. It also has the access to global variables also.

[More on Closure](https://medium.com/@prashantramnyc/javascript-closures-simplified-d0d23fa06ba4)

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Getter & Setter**

We have get and set keyword for functions. We can use those get and setz functions to get and set the value of object property.

But we also have a object feature to get and set property.

```javascript
function Circle(radius) {
    this.radius = radius;
    
    let defaultLocation = {x: 0, y: 0};
    
    let computeOptimumLocation = function(factor) {
        //...
    }
    
    this.draw = function() {
        
        computeOptimumLocation(0.1);
        //defaultLocation
        
        //But properties associated with CIrcle Object can only be accessed with 'this' keyword.
        //this.radius
        
        console.log('draw');
    };
    
    Object.defineProperty(this, 'defaultLocation', {
        get: function() {
            return defaultLocation;
        }
        
        set: function(value) {
            if (!value.x || !value.y)
                throw new Error('Invalid location.');
            defaultLocation = value;
        }
    })
}

const circle = new Circle(10);
//get value
circle.defaultLocation

//set value
circle.defaultLocation = {x: 1, y: 1};
circle.draw();
```

**[⬆ back to top](#markdown-header-table-of-contents)**

## ES6 Classes

* #### **ES6 Classes**

```javascript
    class Circle {
        constructor(radius) {
        this.radius = radius;
        }
        
        draw() {
        console.log('draw');
        }
    }

    const c = new Circle(1);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Static Methods**

There are 2 types of methods i.e instance methods and static methods. Instance methods are accessible and call by class instances (Objects) while static methods are accessbile and call by Class reference it self.

```javascript
    class Circle {
        constructor(radius) {
        this.radius = radius;
        }
        
        //Instance method
        draw() {
        console.log('draw');
        }
        
        //Static method
        static parse(str) {
            const radius = JSON.parse(str).radius;
            return new Circle(radius);
        }
    }

    const circle = Circle.parse('{"radius": 1}')
    console.log(circle);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **This Keyword**

```javascript
    'use strict';
    
    const Circle = function() {
        this.draw = function(() {
            console.log(this);
        }
    };
    
    const c  = new Circle();
    
    c.draw(); 
    //output => circle object
    
    const draw = c.draw;
    
    draw();
    //output => window object //if we dont use strict mode
    //output => undefined //if we use strict mode
    
    //Methods in ES6 Classes run in strit mode by default
    class Circle {
        draw() {
        console.log(this);
        }
    }

    const c = new Circle();
    const draw = c.draw;
    draw()
    //output => undefined
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Private Members With Symbols**

Symbols are not fully private.

```javascript
    const _radius = Symbol();
    const _draw = Symbol();
    
    class Circle {
        constructor(radius) {
            this[_radius] = radius;
        }
        
        [_draw]() {
        }
    }
    
    const c = new Circle(1);
    const key = Object.getOwnPropertySymbols(c)[0];
    console.log(c[key]);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Private Members With WeakMap**

If we private members using weak map, we can still access the private members if we can have access to the weakmap.
And we can restrict the access of weakmap using ES6 Modules.

In WeakMaps if we dont have a refernce to key, it will be collected by a garbage collector. Thats why they are called weak map.

```javascript
    const _radius = new WeakMap();
    const _move = new WeakMap();
    
    class Circle {
        constructor(radius) {
            _radius.set(this, radius);
            
            _move.set(this, function(){
                console.log('move', this);
            });
        }
        
        draw() {
            console.log(_radius.get(this));
            _move.get(this)();
        }
    }
    
    const c = new Circle(1);
    
    c.draw()
    //output
    //1
    //move undefined
    
    //As we have callback funstion in move fucntion so the 'this' keyword becomes the
    //part of the regular funtion and rebounds to global function and as we know class
    //runs in a strict mode thats why we got undefined.
    //we have seaveral solutions for this problem including making the callback function
    //as arrow function beecause arrow functions uses the 'this' reference of their
    //containing function
    
    const _radius = new WeakMap();
    const _move = new WeakMap();
    
    class Circle {
        constructor(radius) {
            _radius.set(this, radius);
            
            _move.set(this, () => {
                console.log('move', this);
            });
        }
        
        draw() {
            console.log(_radius.get(this));
            _move.get(this)();
        }
    }
        
        const c = new Circle(1);
    
        c.draw()
        //output
        //1
        //move circle {}
```

We can create a single weak map for all the properties as follow

```javascript
    const privateProps = new WeakMap();
    class Circle {
        constructor(radius) {
            privateProps.set(this, {
                radius: radius,
                move: ()=> {
                }
            })
        }
        
        privateProps.get(this).radius;
    }
        
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Getter & Setter**

```javascript
    const _radius = new WeakMap();
    
    class Circle {
        constructor(radius) {
            _radius.set(this, radius);
        }
        
        get radius() {
            return _radius.get(this);
        }
        
        set radius(value) {
            if(value <= 0) throw new Error('invalid radius');
            _radius.set(this, value);
        }
    }
    
    const c = new Circle(1);
    c.radius //1
    c.radius = 10;
    c.radius //10
    c.radius = -1; //error
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Inheritance**

```javascript
    class Shape {
        constructor(color) {
            this.color = color;
        }
        
        move() {
            console.log('move');
        }
    }
    
    class Circle extends Shape {
        constructor(color, radius) {
            super(color);
            this.radius = radius;
        }
        
        draw() {
            console.log('draw');
        }
    }
    
    const c = new Circle('red', 1);
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **Method Overriding**

```javascript
    class Shape {
        move() {
            console.log('move');
        }
    }
    
    class Circle extends Shape {
        move() {
            super.move(); //consoles move (from shape class)
            console.log('circle move');
        }
    }
    
    const c = new Circle();
```

**[⬆ back to top](#markdown-header-table-of-contents)**

## Modules

* #### **CommonJS Modules**

This format is used in Node.js
```javascript
index.js
const Circle = require('./circle');
    const c = new Circle(10);

    c.draw()
    //output
    //Circle with radius 10

circle.js

const _radius = new WeakMap();
    
    class Circle {
        constructor(radius) {
            _radius.set(this, radius);
            
        }
        
        draw() {
            console.log('Circle with radius ;+_radius.get(this));
        }
    }
//if we have multiple classes in circle.js file to export, we will export like this
//module.exports.Circle = Circle;
//modules.exports.Square = Square

//for a single class to be export, we will export like this
module.exports = Circle;
```

**[⬆ back to top](#markdown-header-table-of-contents)**

* #### **ES6 Modules**

This format is used for browser in frontend frameworks
```javascript
index.js
import {Circle} from './circle'
    const c = new Circle(10);

    c.draw()
    //output
    //Circle with radius 10

circle.js

const _radius = new WeakMap();
    
    export class Circle {
        constructor(radius) {
            _radius.set(this, radius);
            
        }
        
        draw() {
            console.log('Circle with radius ;+_radius.get(this));
        }
    }
```

**[⬆ back to top](#markdown-header-table-of-contents)**

